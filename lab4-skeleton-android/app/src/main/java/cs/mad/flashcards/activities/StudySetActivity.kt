package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class StudySetActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStudySetBinding

    var data: List<Flashcard> = Flashcard.getHardcodedFlashcards()
    var cards = data.toMutableList()
    var missedCards = mutableListOf<Flashcard>()
    var index = 0
    var isClicked = false
    var missed = 0
    var correct = 0
    var completed = 0
    var total = cards.count()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        binding = ActivityStudySetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.exitButton.setOnClickListener {
            finish()
        }

        binding.missedButton.setOnClickListener {
            if(!cards.isEmpty()){
                var element = cards.removeAt(index)
                cards.add(element)
                missedCards.add(element)

                missed += 1
                isClicked = false
                showFlashcard()
            }
        }

        binding.skipButton.setOnClickListener {
            if(!cards.isEmpty()){
                var element = cards.removeAt(index)
                cards.add(element)

                isClicked = false
                showFlashcard()
            }
        }

        binding.correctButton.setOnClickListener {
            if(!cards.isEmpty()){
                var beenMiss = false
                for(element in missedCards) {
                    if(cards[index] == element){
                        beenMiss = true
                    }
                }

                if(beenMiss == false){
                    correct += 1
                }

                completed += 1
                isClicked = false
                cards.removeAt(index)
                showFlashcard()
            } else{
                isClicked = false
                completed = total
                binding.termDefinition.text = "set completed"
                showFlashcard()
            }
        }

        showFlashcard()
    }

    fun showFlashcard() {
        binding.missed.text = missed.toString()
        binding.correct.text = correct.toString()
        binding.completed.text = completed.toString()

        if(isClicked == false && !cards.isEmpty()){
            binding.termDefinition.text = cards[index].question
        } else if(isClicked == true && !cards.isEmpty()){
            binding.termDefinition.text = cards[index].answer
        }
    }


}