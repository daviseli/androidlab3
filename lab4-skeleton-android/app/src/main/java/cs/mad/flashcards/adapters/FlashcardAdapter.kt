package cs.mad.flashcards.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.databinding.EditTextBinding
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private lateinit var dialogBinding: EditTextBinding
    private val dataSet = dataSet.toMutableList()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        dialogBinding = EditTextBinding.inflate(LayoutInflater.from(viewGroup.context))
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question
        viewHolder.itemView.setOnClickListener{
            AlertDialog.Builder(viewHolder.itemView.context)
                .setTitle(item.question)
                .setMessage(item.answer)
                .setPositiveButton("Edit") {_ , _ -> dialogBinding.input1.hint = item.question; dialogBinding.input2.hint = item.answer; AlertDialog.Builder(viewHolder.itemView.context)
                    .setTitle(item.question)
                    .setMessage(item.answer)
                    .setView(dialogBinding.inputs)
                    .setPositiveButton("Done") {_ , _ -> if(!dialogBinding.input1.text.toString().isBlank()){dataSet[position].question = dialogBinding.input1.text.toString()}; if(!dialogBinding.input2.text.toString().isBlank()){dataSet[position].answer = dialogBinding.input2.text.toString()}; notifyDataSetChanged()}
                    .setNegativeButton("Delete") {_ , _ -> dataSet.removeAt(position); notifyDataSetChanged()}
                    .show()
                }
                .setNeutralButton("Done") {_ , _ ->}
                .setNegativeButton("Delete") {_ , _ -> dataSet.removeAt(position); notifyDataSetChanged()}
                .show()
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }
}